﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

    public class ZGXDatetime
    {
        public static int hflag = 0;                //标识操作节日窗体的显示名称
        public static int rflag = 0;                //标识操作提醒窗体的显示名称
        //定义星座数组
        private string[] ConstellationName =
        {
            "白羊座","金牛座","双子座",
            "巨蟹座","狮子座","处女座",
            "天枰座","天蝎座","射手座",
            "摩羯座","水瓶座","双鱼座"
        };
        private DateTime m_Date;
        public DateTime Date
        {
            get { return m_Date; }
            set { m_Date = value; }
        }
        #region 类的构造函数
        public ZGXDatetime()
        {
            Date = DateTime.Today;
        }
        public ZGXDatetime(DateTime dt)
        {
            Date = dt.Date;
        }
        #endregion
        #region 计算指定日期的星座序号
        ///<summary>
        ///计算指定日期的星座序号
        ///</summary>
        ///<param name="dt">指定的日期</param>
        ///<returns>星座序列号</returns>
        public int GetConstellation(DateTime dt)
        {
            int Y, M, D;
            Y = dt.Year;
            M = dt.Month;
            D = dt.Day;
            Y = M * 100 + D;
            if (((Y >= 321) && (Y <= 419))) { return 0; }
            else if (((Y >= 420) && (Y <= 520))) { return 1; }
            else if (((Y >= 521) && (Y <= 620))) { return 2; }
            else if (((Y >= 621) && (Y <= 722))) { return 3; }
            else if (((Y >= 723) && (Y <= 822))) { return 4; }
            else if (((Y >= 823) && (Y <= 922))) { return 5; }
            else if (((Y >= 923) && (Y <= 1022))) { return 6; }
            else if (((Y >= 1023) && (Y <= 1121))) { return 7; }
            else if (((Y >= 1122) && (Y <= 1221))) { return 8; }
            else if (((Y >= 1222) || (Y <= 119))) { return 9; }
            else if (((Y >= 120) && (Y <= 218))) { return 10; }
            else if (((Y >= 219) && (Y <= 320))) { return 11; }
            else { return -1; };
        }
        #endregion
        #region 计算指定日期的星座名称
        ///<summary>
        ///计算指定日期的星座名称
        ///</summary>
        ///<param name="dt">指定的日期</param>
        ///<returns>星座名</returns>
        public string GetConstellationName(DateTime dt)
        {
            int Constellation;
            Constellation = GetConstellation(dt);
            if ((Constellation >= 0) && (Constellation <= 11))
            { return ConstellationName[Constellation]; }
            else { return ""; }
        }
        #endregion
        //天干
        private static string[] TianGan = { "甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸" };
        //地支
        private static string[] DiZhi = { "子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥" };
        //十二生肖
        private static string[] ShengXiao = { "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪" };
        /*农历日期名*/
        private static string[] DayName ={"*","初一","初二","初三","初四","初五",
       "初六","初七","初八","初九","初十","十一","十二","十三","十四","十五","十六","十","十八","十九","二十","廿一","廿二","廿三","廿四","廿五","廿六","廿七","廿八","廿九","三十"};
        /*农历月份名*/
        private static string[] MonthName = { "*", "正", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "腊" };
        /*公历每月前面的天数*/
        private static int[] MonthAdd = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 };
        #region 农历数据
        ///<summary>
        ///农历数据
        ///</summary>
        private static int[] LunarData = {2635,333387,1701,1748,267701,694,2391,133423,1175,396438
       ,3402,3749,331177,1453,694,201326,2350,465197,3221,3402
       ,400202,2901,1386,267611,605,2349,137515,2709,464533,1738
       ,2901,330421,1242,2651,199255,1323,529706,3733,1706,398762
       ,2741,1206,267438,2647,1318,204070,3477,461653,1386,2413
       ,330077,1197,2637,268877,3365,531109,2900,2922,398042,2395
       ,1179,267415,2635,661067,1701,1748,398772,2742,2391,330031
       ,1175,1611,200010,3749,527717,1452,2742,332397,2350,3222
       ,268949,3402,3493,133973,1386,464219,605,2349,334123,2709
       ,2890,267946,2773,592565,1210,2651,395863,1323,2707,265877};
        #endregion
        #region 获取对应日期的农历及其他信息
        ///<summary>
        ///获取对应日期的农历及其他信息
        ///</summary>
        ///<param name="dtDay">公历日期</param>
        ///<returns>农历信息</returns>
        public string GetLunarCalendar(DateTime dtDay)
        {
            string sYear = dtDay.Year.ToString();
            string sMonth = dtDay.Month.ToString();
            string sDay = dtDay.Day.ToString();
            int year, month, day;
            try
            {
                year = int.Parse(sYear);
                month = int.Parse(sMonth);
                day = int.Parse(sDay);
            }
            catch
            {
                year = DateTime.Now.Year;
                month = DateTime.Now.Month;
                day = DateTime.Now.Day;

            }
            int nTheDate, nIsEnd;
            int k, m, n, nBit, i;
            string calendar = string.Empty;
            //计算到初始时间1921年2月8日的天数：1921-2-8(正月初一)
            nTheDate = (year - 1921) * 365 + (year - 1921) / 4 + day + MonthAdd[month - 1] - 38;
            if ((year % 4 == 0) && (month > 2)) nTheDate += 1;
            //计算天干、地支、月、日
            nIsEnd = 0;
            m = 0;
            k = 0;
            n = 0;
            while (nIsEnd != 1)
            {
                if (LunarData[m] < 4095) k = 11;
                else k = 12;
                n = k;
                while (n >= 0)
                {
                    //获取LunarData[m]的第n个二进制的值
                    nBit = LunarData[m];
                    for (i = 1; i < n + 1; i++) nBit = nBit / 2;
                    nBit = nBit % 2;
                    if (nTheDate <= (29 + nBit))
                    {
                        nIsEnd = 1;
                        break;
                    }
                    nTheDate = nTheDate - 29 - nBit;
                    n = n - 1;
                }
                if (nIsEnd == 1) break;
                m = m + 1;
            }
            year = 1921 + m;
            month = k - n + 1;
            day = nTheDate;
            //return year="-"+month+"-"+day;
            #region 格式化日期显示为三月廿四
            if (k == 12)
            {
                if (month == LunarData[m] / 65536 + 1) month = 1 - month;
                else if (month > LunarData[m] / 65536 + 1) month = month - 1;
            }
            //生肖
            calendar = ShengXiao[(year - 4) % 60 % 12].ToString() + "年";
            //天干
            calendar += TianGan[(year - 4) % 60 % 10].ToString();
            //地支
            calendar += DiZhi[(year - 4) % 60 % 12].ToString() + " ";
            //农历月
            if (month < 1)
                calendar += "闰" + MonthName[-1 * month].ToString() + "月";
            else calendar += MonthName[month].ToString() + "月";
            //农历月
            calendar += DayName[day].ToString();
            return calendar;
            #endregion
        }
        #endregion
    }
