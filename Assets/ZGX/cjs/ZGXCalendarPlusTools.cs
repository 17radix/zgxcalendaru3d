﻿using UnityEngine;
using System.Collections;
using System;
public class ZGXCalendarPlusTools {
    public GameObject goList;
    public static DayOfWeek GetFirstDayInMonthOfWeek(DateTime time)
    {
        return new DateTime(time.Year, time.Month, 1).DayOfWeek;
    }
	public static  bool CompareWithMonth(DateTime time1, DateTime time2)
	{
		if (time1.Month == time2.Month && time1.Year == time2.Year) {
			return true;
		} else {
			return false;
		}
	}
}
