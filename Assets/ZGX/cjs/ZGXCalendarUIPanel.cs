﻿using UnityEngine;
using System.Collections;
using System;

public class ZGXCalendarUIPanel : MonoBehaviour {
    ZGXCalendarUIPanelLattice[,] rectgrid;
    public Color DarkColor;
    public Color LightColor;
    [SerializeField]
    private Transform gridNode;
    [SerializeField]
    private ZGXCalendarUIPanelLattice LatticePb;
    [SerializeField]
    private RectTransform rect;
    [SerializeField]
    private float Padding;
    //
    private DateTime nowFocusTime;
    public DateTime NowFocusTime
    {
        get
        {
            return nowFocusTime;
        }
        set
        {
			if (!ZGXCalendarPlusTools.CompareWithMonth(value,nowFocusTime)) {
				nowFocusTime = value;
				DateInit (value);
			} else {
				nowFocusTime = value;
			}
           
        }
    }

    public ZGXCalendarUIPanelLattice GetLattice(DateTime date)
    {
        if (NowFocusTime.Month != date.Month)
        {
            return null;
        }
        else
        {
            return rectgrid[(date.Day+ (int)ZGXCalendarPlusTools.GetFirstDayInMonthOfWeek(date))-1 / 7,(int)date.DayOfWeek];
        }
    }
    // 
	private void init(DateTime today,String ICSfile)
    {

    }
	private void init(DateTime time)
    {
        if (rectgrid == null)
            BuildGrid();
		NowFocusTime = time;
    }
    // Use this for initialization
    void Start () {
        init(DateTime.Today);
    }

    private void BuildGrid()
    {
        rectgrid = new ZGXCalendarUIPanelLattice[7,7];
        float grad = (1.0f - Padding * 7) / 6;
        float grad1 = (1.0f - Padding * 8) / 7;
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (rectgrid[i,j] == null)
                {
                    rectgrid[i,j] = Instantiate(LatticePb) as ZGXCalendarUIPanelLattice;
                    RectTransform tpRect = rectgrid[i,j].gameObject.GetComponent<RectTransform>();
                    tpRect.SetParent(gridNode);
                    tpRect.anchorMin = new Vector2(Padding*(j+1)+ grad1 * j, 1- Padding * (i + 1) - (grad * (i+1)));
                    tpRect.anchorMax = new Vector2(Padding * (j + 1) + grad1 * (j+1), 1 - Padding * (i + 1) - (grad * i));
                    tpRect.offsetMax = Vector2.zero;
                    tpRect.offsetMin = Vector2.zero;
                }
            }
        }
    }
    private void DateInit(DateTime time)
    {
        DateTime temptime = new DateTime(time.Year, time.Month, 1);
        int t = (int)ZGXCalendarPlusTools.GetFirstDayInMonthOfWeek(time);
        if (t == 0)
        {
            t = 7;
        }
        temptime=temptime.AddDays(-t);
        int tempmo = time.Month;
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (rectgrid[i, j] != null)
                {
                        rectgrid[i, j].SetDateTime(temptime);
					if (!ZGXCalendarPlusTools.CompareWithMonth(temptime,nowFocusTime)) {
						rectgrid [i, j].Able = false;
					}
                        temptime= temptime.AddDays(1);
                    }   
                }
            }
        }
    
    // Update is called once per frame
    void Update () {
	
	}
}
