﻿using System;
using UnityEngine;
using UnityEngine.UI;

    public class ZGXCalendarUIPanelLattice: MonoBehaviour
    {
        [SerializeField]
        private Text DateText;
        [SerializeField]
        private Text SubtitleText;
        [SerializeField]
        private Image MainImg;
        [SerializeField]
        private Text MainText;
        [SerializeField]
        private Button btn;
        [SerializeField]
        private Image BackImg;
	public bool Able
	{
		set 
		{
			btn.interactable = value;
		}
		get
		{
			return btn.interactable;
		}
	}
        public Color BackColor
        {
            set
            {
                BackImg.color = value;
            }
        }
        public Color ImageColor
        {
            set
            {
                MainImg.color = value;
            }
        }
        public Color TextColor
        {
            set
            {
                MainText.color = value;
                SubtitleText.color = value;
            }
        }
        public void Click()
        {
            if (evClick != null)
            {
                evClick();
            }
        }
        public Action evClick;
        public void SetImage(Texture2D texture)
        {
            if (texture != null)
            {
                MainImg.color = Color.white;
            }
            else
            {
                MainImg.color = new Color(0, 0, 0, 0);
            }
        }
        public void SetDateTime(DateTime date)
        {
            DateText.text = date.Day.ToString();
            //ZGXChineseLunisolarCalendar
            //SubtitleText.text = ZGXChineseLunisolarCalendar.GetChineseDateDay(date);
        }
    public void ResetVoid()
    {
        DateText.text = "";
        SubtitleText.text = "";
        SetImage(null);
        MainText.text = "";
    }
}
